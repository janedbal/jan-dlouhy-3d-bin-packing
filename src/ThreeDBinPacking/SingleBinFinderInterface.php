<?php

namespace App\ThreeDBinPacking;

use App\DataObject\Bin;
use App\SingleBinPacking\SingleBinPackingRequest;

interface SingleBinFinderInterface
{
    // missing @throws annotations and unified exceptions for the interface
    public function find(SingleBinPackingRequest $request): Bin;
}
