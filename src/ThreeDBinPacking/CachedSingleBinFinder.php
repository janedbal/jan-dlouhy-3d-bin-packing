<?php

declare(strict_types=1);

namespace App\ThreeDBinPacking;

use App\DataObject\Bin;
use App\DataObject\Product;
use App\SingleBinPacking\SingleBinPackingRequest;
use Doctrine\Common\Cache\Cache;

final class CachedSingleBinFinder implements SingleBinFinderInterface
{
    private SingleBinFinder $singleBinFinder;

    private Cache $cache;

    // construct needs to accept the interface instead of the specific type if this should work as a decorator
    public function __construct(SingleBinFinder $singleBinFinder, Cache $cache)
    {
        $this->singleBinFinder = $singleBinFinder;
        $this->cache = $cache;
    }

    public function find(SingleBinPackingRequest $request): Bin
    {
        $cacheKey = $this->getCacheKey($request);

        if ($this->cache->contains($cacheKey)) {
            return $this->cache->fetch($cacheKey);
        }

        $bin = $this->singleBinFinder->find($request);

        // Caching PHP object, this is not kind of cache you want to flush on every deplyoment (there is no reason for it). Any change on the object will make cahce invalid.
        $this->cache->save($cacheKey, $bin);

        return $bin;
    }

    private function getCacheKey(SingleBinPackingRequest $request): string
    {
        // This doesn't handle variants of products with same dims in different order (4x4x1 === 1x4x4)
        $productsKeys = array_map(function(Product $product) {
            return serialize($product);
        }, $request->getProducts());

        sort($productsKeys);

        return serialize($productsKeys);
    }
}
