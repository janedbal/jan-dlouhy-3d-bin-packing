<?php

declare(strict_types=1);

namespace App\DataObject;

final class Bin
{
    private string $id;

    private float $width;

    private float $height;

    private float $length;

    public function __construct(string $id, float $width, float $height, float $length)
    {
        $this->id = $id;
        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['w'],
            $data['h'],
            $data['d'],
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getWidth(): float
    {
        return $this->width;
    }

    public function getHeight(): float
    {
        return $this->height;
    }

    public function getLength(): float
    {
        return $this->length;
    }
}
